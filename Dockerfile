FROM dockerfile/nodejs

ADD package.json /tmp/
ADD npm-shrinkwrap.json /tmp/

WORKDIR /tmp/

RUN npm install

ADD index.js /tmp/

CMD node index
