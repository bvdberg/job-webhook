var request = require('request');
var util = require('util');

var url = process.env.URI;
var payload = process.env.PAYLOAD;

var options = {
	'body': {
		'foo': payload,
		'now': new Date()
	},
	'json': true,
	'method': 'POST'
};

request(url, options, function(err) {
	if (err) {
		console.error('Unable to send webhook');
		console.error(util.inspect(err));
		process.exit(1);
	}
	else {
		console.log('done!');
	}
});
